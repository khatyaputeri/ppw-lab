from django.test import TestCase, Client
from django.urls import resolve
from .views import index, profile
from .csui_helper import get_access_token,verify_user, get_client_id, get_data_user
from .api_enterkomputer import get_drones, get_opticals, get_soundcards
import requests
import os
import environ

class Lab9UnitTest(TestCase):
	def setUp(self):
		root = environ.Path(__file__) - 3 # three folder back (/a/b/c/ - 3 = /)
		env = environ.Env(DEBUG=(bool, False),)
		self.username = env("SSO_USERNAME")
		self.password = env("SSO_PASSWORD")
		
	def test_url_is_exist(self):
		response = Client().get('/lab-9/')
		self.assertEqual(response.status_code, 200)

	def test_is_using_index_func(self):
		found = resolve('/lab-9/')
		self.assertEqual(found.func, index)

	def test_template_using_lab_9_html(self):
		response = Client().get('/lab-9/')
		self.assertTemplateUsed(response, "lab_9/layout/base.html")

	#for csui_helper
	def test_client_id(self):
		id = get_client_id()
		self.assertEqual('X3zNkFmepkdA47ASNMDZRX3Z9gqSU1Lwywu5WepG', id)

	def test_get_access_token_and_data(self):
		root = environ.Path(__file__) - 3 # three folder back (/a/b/c/ - 3 = /)
		env = environ.Env(DEBUG=(bool, False),)
		uname = env("SSO_USERNAME")
		sandi = env("SSO_PASSWORD")
		accToken = get_access_token(uname,sandi)
		if(accToken != None):
			verify_user(accToken)
			get_data_user(accToken, '1')
		self.assertNotEqual(accToken, None)
		accTokenNone = get_access_token("khatya", "ngasal")
		self.assertEqual(accTokenNone, None)

	#for api enterkomputer
	def test_get_drone(self):
		#drones = requests.get('https://www.enterkomputer.com/api/product/drone.json')
		getDrone = get_drones()
		self.assertNotEqual(getDrone, None)

	def test_get_optical(self):
		#drones = requests.get('https://www.enterkomputer.com/api/product/drone.json')
		getOptical = get_opticals()
		self.assertNotEqual(getOptical, None)

	def test_get_soundcard(self):
		#drones = requests.get('https://www.enterkomputer.com/api/product/drone.json')
		getSoundcard = get_soundcards()
		self.assertNotEqual(getSoundcard, None)

	#for custom auth
	def test_login(self):
		root = environ.Path(__file__) - 3 # three folder back (/a/b/c/ - 3 = /)
		env = environ.Env(DEBUG=(bool, False),)
		uname = env("SSO_USERNAME")
		sandi = env("SSO_PASSWORD")
		response = self.client.post('/lab-9/custom_auth/login/', data={'username': uname, 'password' : sandi})
		response2 = self.client.post('lab-9/custom_auth/login/', data={'username': uname, 'password' : '123'})

		profile1 = self.client.post('/lab-9/profile/')
		profile2 = resolve('/lab-9/profile/')

		self.assertEqual(response.status_code, 302)
		self.assertEqual(profile2.func, profile)

	def test_failed_login(self):
		response2 = self.client.post('/lab-9/custom_auth/login/', data={'username': 'khatya', 'password' : '123'})
		self.assertEqual(response2.status_code, 302)

	def test_logout(self):
		response = self.client.post('/lab-9/custom_auth/logout/')
		self.assertEqual(response.status_code, 302)

	#for views
	def test_add_session_drones(self):
		response = Client().get('/lab-9/add_session_drones/12343/')
		self.assertEqual(response.status_code, 302)


	def test_del_session_drones(self):
		root = environ.Path(__file__) - 3 # three folder back (/a/b/c/ - 3 = /)
		env = environ.Env(DEBUG=(bool, False),)
		uname = env("SSO_USERNAME")
		sandi = env("SSO_PASSWORD")
		response = self.client.post('/lab-9/custom_auth/login/', data={'username': uname, 'password' : sandi})
		response = self.client.post('/add_session_drones/1234/')
		response2 = self.client.post('/del_session_drones/1234/')
		self.assertEqual(response2.status_code, 404)

	def test_clear_drone(self):
		root = environ.Path(__file__) - 3 # three folder back (/a/b/c/ - 3 = /)
		env = environ.Env(DEBUG=(bool, False),)
		uname = env("SSO_USERNAME")
		sandi = env("SSO_PASSWORD")
		response = self.client.post('/lab-9/custom_auth/login/', data={'username': uname, 'password' : sandi})
		response = self.client.post('/clear_session_drones/')
		self.assertEqual(response.status_code, 404)

	def test_add_session_opticals(self):
		response = Client().get('/lab-9/add_session_opticals/12343/')
		self.assertEqual(response.status_code, 302)


	def test_del_session_opticals(self):
		root = environ.Path(__file__) - 3 # three folder back (/a/b/c/ - 3 = /)
		env = environ.Env(DEBUG=(bool, False),)
		uname = env("SSO_USERNAME")
		sandi = env("SSO_PASSWORD")
		response = self.client.post('/lab-9/custom_auth/login/', data={'username': uname, 'password' : sandi})
		response = self.client.post('/add_session_opticals/1234/')
		response2 = self.client.post('/del_session_opticals/1234/')
		self.assertEqual(response2.status_code, 404)

	def test_clear_opticals(self):
		root = environ.Path(__file__) - 3 # three folder back (/a/b/c/ - 3 = /)
		env = environ.Env(DEBUG=(bool, False),)
		uname = env("SSO_USERNAME")
		sandi = env("SSO_PASSWORD")
		response = self.client.post('/lab-9/custom_auth/login/', data={'username': uname, 'password' : sandi})
		response = self.client.post('/clear_session_opticals/')
		self.assertEqual(response.status_code, 404)

	def test_add_session_soundcards(self):
		response = Client().get('/lab-9/add_session_soundcards/12343/')
		self.assertEqual(response.status_code, 302)


	def test_del_session_soundcards(self):
		root = environ.Path(__file__) - 3 # three folder back (/a/b/c/ - 3 = /)
		env = environ.Env(DEBUG=(bool, False),)
		uname = env("SSO_USERNAME")
		sandi = env("SSO_PASSWORD")
		response = self.client.post('/lab-9/custom_auth/login/', data={'username': uname, 'password' : sandi})
		response = self.client.post('/add_session_soundcards/1234/')
		response2 = self.client.post('/del_session_soundcards/1234/')
		self.assertEqual(response2.status_code, 404)

	def test_clear_soundcards(self):
		root = environ.Path(__file__) - 3 # three folder back (/a/b/c/ - 3 = /)
		env = environ.Env(DEBUG=(bool, False),)
		uname = env("SSO_USERNAME")
		sandi = env("SSO_PASSWORD")
		response = self.client.post('/lab-9/custom_auth/login/', data={'username': uname, 'password' : sandi})
		response = self.client.post('/clear_session_soundcards/')
		self.assertEqual(response.status_code, 404)	

	#cookie
	def test_cookie(self):
		#not logged in
		response = self.client.get('/lab-9/cookie/login/')
		self.assertEqual(response.status_code, 200)
		response = self.client.get('/lab-9/cookie/profile/')
		self.assertEqual(response.status_code, 302)

		#login using HTTP GET method
	def test_login_auth_cookie(self):
		response = self.client.get('/lab-9/cookie/auth_login/')
		self.assertEqual(response.status_code, 302)

		#login failed, invalid pass and uname


		response = self.client.post('/lab-9/cookie/auth_login/', {'username': 'salah', 'password': 'salah'})
		html_response = self.client.get('/lab-9/cookie/login/').content.decode('utf-8')
		self.assertEqual(response.status_code, 302)
		self.assertIn("Username atau Password Salah", html_response)

		#try to set manual cookies
		self.client.cookies.load({"user_login": "salah", "user_password": "salah"})
		response = self.client.get('/lab-9/cookie/profile/')
		html_response = response.content.decode('utf-8')
		self.assertIn("Kamu tidak punya akses :P ", html_response)

		#login successed
		self.client = Client()
		response = self.client.post('/lab-9/cookie/auth_login/', {'username': 'khatya', 'password': 'khat'})
		self.assertEqual(response.status_code, 302)
		response = self.client.get('/lab-9/cookie/login/')
		self.assertEqual(response.status_code, 200)
		response = self.client.get('/lab-9/cookie/profile/')
		self.assertEqual(response.status_code, 302)

		#logout
		response = self.client.post('/lab-9/cookie/clear/')
		self.assertEqual(response.status_code, 302)


	def test_def_profile_in_views(self):
		response = self.client.post('/lab-9/custom_auth/login/', {'username': self.username, 'password': self.password})
		self.assertEqual(response.status_code, 302)
		response = self.client.get('/lab-9/profile/')
		self.assertEqual(response.status_code, 200)

		#tanpa login, kita logout kan dia dulu
		response = self.client.post('/lab-9/custom_auth/logout/')
		pesan = self.client.get('/lab-9/').content.decode('utf-8')
		self.assertEqual(response.status_code, 302)
		self.assertIn("Anda berhasil logout. Semua session Anda sudah dihapus", pesan)
		response = self.client.get('/lab-9/profile/')
		self.assertEqual(response.status_code, 302)

	def test_add_drones_di_views(self):
		self.client.post('/lab-9/custom_auth/login/', {'username': self.username, 'password': self.password})
		#add drone
		response = self.client.post('/lab-9/add_session_drones/'+get_drones().json()[0]["id"]+'/')
		response = self.client.post('/lab-9/add_session_drones/'+get_drones().json()[1]["id"]+'/')
		html_response = self.client.get('/lab-9/profile/').content.decode('utf8')
		self.assertEqual(response.status_code, 302)
		self.assertIn("Berhasil tambah drone favorite", html_response)

		#delete drone
		response = self.client.post('/lab-9/del_session_drones/'+get_drones().json()[0]["id"]+'/')
		html_response = self.client.get('/lab-9/profile/').content.decode('utf8')
		self.assertEqual(response.status_code, 302)
		self.assertIn("Berhasil hapus dari favorite", html_response)

		#reset drones
		response = self.client.post('/lab-9/clear_session_drones/')
		html_response = self.client.get('/lab-9/profile/').content.decode('utf8')
		self.assertEqual(response.status_code, 302)
		self.assertIn("Berhasil reset favorite drones", html_response)

	def test_add_soundcards_di_views(self):
		self.client.post('/lab-9/custom_auth/login/', {'username': self.username, 'password': self.password})
		#add drone
		response = self.client.post('/lab-9/add_session_soundcards/'+get_soundcards().json()[0]["id"]+'/')
		response = self.client.post('/lab-9/add_session_soundcards/'+get_soundcards().json()[1]["id"]+'/')
		html_response = self.client.get('/lab-9/profile/').content.decode('utf8')
		self.assertEqual(response.status_code, 302)
		#self.assertIn("Berhasil tambah soundcards favorite", html_response)

		#delete drone
		response = self.client.post('/lab-9/del_session_soundcards/'+get_soundcards().json()[0]["id"]+'/')
		html_response = self.client.get('/lab-9/profile/').content.decode('utf8')
		self.assertEqual(response.status_code, 302)
		self.assertIn("Berhasil hapus dari favorite", html_response)

		#reset drones
		response = self.client.post('/lab-9/clear_session_soundcards/')
		html_response = self.client.get('/lab-9/profile/').content.decode('utf8')
		self.assertEqual(response.status_code, 302)
		self.assertIn("Berhasil reset favorite soundcards", html_response)

		#ini sama persisi kayak drones del
	def test_add_opticals_di_views(self):
		self.client.post('/lab-9/custom_auth/login/', {'username': self.username, 'password': self.password})
		#add drone
		response = self.client.post('/lab-9/add_session_opticals/'+get_opticals().json()[0]["id"]+'/')
		response = self.client.post('/lab-9/add_session_opticals/'+get_opticals().json()[1]["id"]+'/')
		html_response = self.client.get('/lab-9/profile/').content.decode('utf8')
		self.assertEqual(response.status_code, 302)
		#self.assertIn("Berhasil tambah opticals favorite", html_response)

		#delete drone
		response = self.client.post('/lab-9/del_session_opticals/'+ get_opticals().json()[0]["id"]+'/')
		html_response = self.client.get('/lab-9/profile/').content.decode('utf8')
		self.assertEqual(response.status_code, 302)
		self.assertIn("Berhasil hapus dari favorite", html_response)

		#reset drones
		response = self.client.post('/lab-9/clear_session_opticals/')
		html_response = self.client.get('/lab-9/profile/').content.decode('utf8')
		self.assertEqual(response.status_code, 302)
		self.assertIn("Berhasil reset favorite opticals", html_response)


			
