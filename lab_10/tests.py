from django.test import TestCase, Client
from django.urls import resolve
from .views import index#, profile
from .omdb_api import search_movie, get_detail_movie, create_json_from_dict
#from .csui_helper import get_access_token,verify_user, get_client_id, get_data_user
import os
import environ

# Create your tests here.
class Lab10UnitTest(TestCase):
	def setUp(self):
		root = environ.Path(__file__) - 3 # three folder back (/a/b/c/ - 3 = /)
		env = environ.Env(DEBUG=(bool, False),)
		self.username = env("SSO_USERNAME")
		self.password = env("SSO_PASSWORD")

	def test_url_is_exist(self):
		response = Client().get('/lab-10/')
		self.assertEqual(response.status_code, 200)

	def test_is_using_index_func(self):
		found = resolve('/lab-10/')
		self.assertEqual(found.func, index)

	def test_template_using_base_html(self):
		response = Client().get('/lab-10/')
		self.assertTemplateUsed(response, "lab_10/layout/base.html")

	def test_login(self):
		response = self.client.post('/lab-10/custom_auth/login/', data={'username': self.username, 'password' : self.password})
		profile1 = self.client.post('/lab-10/dashboard/')

		self.assertEqual(response.status_code, 302)

	def test_login_failed(self):
		response2 = self.client.post('/lab-10/custom_auth/login/', data={'username': self.username, 'password' : '123'})
		self.assertEqual(response2.status_code, 302)

	def test_omdb(self):
		x = search_movie("it", "2017")
		y = get_detail_movie("12312")
		self.assertNotEqual(None, x)
		self.assertNotEqual(None, y)

	def test_logout(self):
		response = self.client.post('/lab-10/custom_auth/logout/')
		self.assertEqual(response.status_code, 302)

	def test_dashboard(self):
		response = self.client.post('/lab-10/custom_auth/login/', data={'username': self.username, 'password' : self.password})
		response2 = self.client.post('/lab-10/dashboard/')

		self.assertEqual(response2.status_code, 200)

	def test_open_movie_list(self):
		response =  self.client.post('/lab-10/custom_auth/login/', data={'username': self.username, 'password' : self.password})
		response2 = self.client.post('/lab-10/movie/list/')
		self.assertEqual(response2.status_code, 200)

	#def test_open_watch_later(self):
		#response =  self.client.post('/lab-10/custom_auth/login/', data={'username': self.username, 'password' : self.password})
		#response2 = self.client.post('/lab-10/movie/watch_later/')
		#self.assertEqual(response2.status_code, 200)

