from django.test import TestCase, Client
from django.urls import resolve
from .views import index, validate_npm
from json import JSONEncoder

# Create your tests here.
class Lab7UnitTest(TestCase):
	def test_lab_7_url_is_exist(self):
		response = Client().get('/lab-7/')
		self.assertEqual(response.status_code, 200)

	def test_lab7_using_index_func(self):
		respon = resolve('/lab-7/')
		self.assertEqual(respon.func, index)

	def test_lab7_template_using_lab7_html(self):
		response = Client().get('/lab-7/')
		self.assertTemplateUsed(response, "lab_7/lab_7.html")

	def test_can_add_friend(self):
		response = self.client.post('/lab-7/add-friend/', data={'name': 'khatya', 'npm' : 123321})
		#self.assertEqual('khatya', response.get['name'])
		#self.assertEqual('123321', response.get['npm'])
		self.assertEqual(response.status_code, 200)

	def test_can_delete_friend(self):
		respons = self.client.post('/lab-7/add-friend/', data={'name': 'khatya', 'npm' : 123})
		response = Client().get('/lab-7/delete-friend/123')
		self.assertEqual(response.status_code, 301)

	def test_data_friend_template_using_teman_html(self):
		response = self.client.post('/lab-7/add-friend/', data={'name': 'khatya', 'npm' : 123})
		response2 = Client().get('/lab-7/friend/123/')
		self.assertTemplateUsed(response2, "lab_7/teman.html")

	def test_get_friend_list_using_template_daftar_teman_html(self):
		response = Client().get('/lab-7/get-friend-list/')
		self.assertTemplateUsed(response, "lab_7/daftar_teman.html")

	def test_not_found(self):
		response = Client().get('/lab-7/validate-npm/12343/')
		self.assertEqual(response.status_code, 404)

	def test_validate_npm(self):
		respons = self.client.post('/lab-7/add-friend/', data={'name': 'khatya', 'npm' : 12343})	
		response = self.client.get('/lab-7/validate-npm/')
		self.assertEqual(response.status_code, 200)