from django.shortcuts import render
from django.http import HttpResponse, HttpResponseRedirect, JsonResponse
from django.views.decorators.csrf import csrf_exempt
from django.core import serializers
from django.core.paginator import Paginator, EmptyPage, PageNotAnInteger

from .models import Friend
from .api_csui_helper.csui_helper import CSUIhelper
import os
import json

response = {}
csui_helper = CSUIhelper()

def index(request):
    # Page halaman menampilkan list mahasiswa yang ada
    # TODO berikan akses token dari backend dengan menggunakaan helper yang ada
    friend_list = Friend.objects.all()
    mahasiswa_list = csui_helper.instance.get_mahasiswa_list()
    paginator = Paginator(mahasiswa_list, 20)

    page = request.GET.get('page')

    try:
        mahasiswa = paginator.page(page)
    except PageNotAnInteger:
        mahasiswa = paginator.page(1)
    except EmptyPage:	#
        mahasiswa = paginator.page(paginator.num_pages)#

    
    response = {"mahasiswa_list": mahasiswa, "friend_list": friend_list}
    html = 'lab_7/lab_7.html'
    return render(request, html, response)

def friend_list(request):
    friend_list = Friend.objects.all()#
    response['friend_list'] = friend_list#
    html = 'lab_7/daftar_teman.html'#
    return render(request, html, response)#

@csrf_exempt
def add_friend(request):
    if request.method == 'POST':#
        name = request.POST['name']#
        npm = request.POST['npm']#
        if Friend.objects.filter(npm = npm).exists():#
            return HttpResponseNotFound()#
        else:#
            friend = Friend(friend_name=name, npm=npm)#
            friend.save()#
            data = model_to_dict(friend)#
            return HttpResponse(data)#
#    elif request.method == 'GET':
#        name = request.GET['name']
#        npm = request.GET['npm']
#        if Friend.objects.filter(npm = npm).exists():
#            return HttpResponseNotFound()
#        else:
#            friend = Friend(friend_name=name, npm=npm)
#            friend.save()
#            data = model_to_dict(friend)
#            return HttpResponse(data)

def delete_friend(request, friend_id):
    Friend.objects.filter(npm=friend_id).delete()#
    return HttpResponseRedirect('/lab-7/')#

def friend(request, npm):
	friend = Friend.objects.get(npm = npm)#
	response = {'friend' : friend}#
	html = 'lab_7/teman.html'#
	return render(request, html, response)#

@csrf_exempt
def validate_npm(request):
    npm = request.POST.get('npm', None)#
    friend_list =Friend.objects.all()#
    data = {#
        'is_taken': Friend.objects.filter(npm = npm).exists()#
    }#
    return JsonResponse(data)#

def model_to_dict(obj):
    data = serializers.serialize('json', [obj,])#
    struct = json.loads(data)#
    data = json.dumps(struct[0]["fields"])#
    return data#
