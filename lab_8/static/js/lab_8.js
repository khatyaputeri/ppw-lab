    

 window.fbAsyncInit = function() {
  FB.init({
    appId      : '1585520931532697',
    cookie     : true,
    xfbml      : true,
    version    : 'v2.11'
  });


  FB.getLoginStatus(function(response) {
    if (response.status === 'connected') {
      render(true);
    }
    else {
      render(false);
    }
  });
};


(function(d, s, id){
   var js, fjs = d.getElementsByTagName(s)[0];
   if (d.getElementById(id)) {return;}
   js = d.createElement(s); js.id = id;
   js.src = "https://connect.facebook.net/en_US/sdk.js";
   fjs.parentNode.insertBefore(js, fjs);
 }(document, 'script', 'facebook-jssdk'));
  
function facebookLogin(){
     FB.login(function(response){
       console.log(response);
       console.log("haha");
       render(true);
     }, {scope:'public_profile,user_posts,publish_actions'})
   }

const getUserData = (fun) => {
  console.log("masuk func getUserData");
   FB.getLoginStatus(function(response) {
        if (response.status === 'connected') {
          FB.api('/me?fields=id,name,about,gender,cover,picture,email', 'GET', function(response){
            fun(response);
            console.log(response);
          });
        }
    });
}

const getUserFeed = (fun) => {
      FB.api("me/feed", function(response){
    console.log(response);
    if (response && !response.error) {
      fun(response);
    }
    else
      alert("Failed to get UserFeed");
  });
}


const postFeed = (message) =>{
     FB.api('/me/feed', 'POST', {message:message});
 }

const postStatus = () =>{
  var message = $('#postInput').val();
  postFeed(message);
}


const facebookLogout = () =>{
     FB.getLoginStatus(function(response) {
        if (response.status === 'connected') {
          FB.logout();
        }
     });
 }

  const render = loginFlag => {
    if (loginFlag) {
        console.log("masuk if loginFlag");
      // Jika yang akan dirender adalah tampilan sudah login

      // Memanggil method getUserData (lihat ke bawah) yang Anda implementasi dengan fungsi callback
      // yang menerima object user sebagai parameter.
      // Object user ini merupakan object hasil response dari pemanggilan API Facebook.
      getUserData(user => {
        console.log("masuk get dataUser");
        // Render tampilan profil, form input post, tombol post status, dan tombol logout
        $('#lab8').html(
          '<div class="profile">' +
            '<img class="cover" src="' + user.cover.source + '" alt="cover" />' +
            '<img class="picture" src="' + user.picture.data.url + '" alt="profpic" />' +
            '<div class="data">' +
              '<h1>' + user.name + '</h1>' +
              '<h2>' + user.about + '</h2>' +
              '<h3>' + user.email + ' - ' + user.gender + '</h3>' +
            '</div>' +
          '</div>' +
          '<input id="postInput" type="text" class="post" placeholder="Ketik Status Anda" />' +
          '<button class="postStatus" onclick="postStatus()">Post ke Facebook</button>' +
          '<button class="logout" onclick="facebookLogout()">Logout</button>'
        );

        // Setelah merender tampilan di atas, dapatkan data home feed dari akun yang login
        // dengan memanggil method getUserFeed yang kalian implementasi sendiri.
        // Method itu harus menerima parameter berupa fungsi callback, dimana fungsi callback
        // ini akan menerima parameter object feed yang merupakan response dari pemanggilan API Facebook
        getUserFeed(feed => {
          feed.data.map(value => {
            // Render feed, kustomisasi sesuai kebutuhan.
            if (value.message && value.story) {
              $('#lab8').append(
                '<div class="feed">' +
                  '<h1>' + value.message + '</h1>' +
                  '<h2>' + value.story + '</h2>' +
                '</div>'
              );
            } else if (value.message) {
              $('#lab8').append(
                '<div class="feed">' +
                  '<h1>' + value.message + '</h1>' +
                '</div>'
              );
            } else if (value.story) {
              $('#lab8').append(
                '<div class="feed">' +
                  '<h2>' + value.story + '</h2>' +
                '</div>'
              );
            }
          });
        });
      });
    } else {
      // Tampilan ketika belum login
      $('#lab8').html('<button class="login" onclick="facebookLogin()">Login</button>');
    }
  };


