from django.test import TestCase, Client
from django.urls import resolve
from .views import index

class Lab8ViewsUnitTest(TestCase):
	def test_url_is_exist(self):
		response = Client().get('/lab-8/')
		self.assertEqual(response.status_code, 200)

	def test_is_using_index_func(self):
		found = resolve('/lab-8/')
		self.assertEqual(found.func, index)

	def test_template_using_lab_8_html(self):
		response = Client().get('/lab-8/')
		self.assertTemplateUsed(response, "lab_8/lab_8.html")
