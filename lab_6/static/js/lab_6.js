var print = document.getElementById('print');
var erase = false;

var go = function(x) {
  if (x === 'ac') {
    	print.value = ''
  } else if (x === 'eval') {
    	print.value = Math.round(evil(print.value) * 10000) / 10000;
    	erase = true;
  } else if (x === 'sin'){
  		print.value = Math.sin(print.value);
  } else if (x === 'tan'){
  		print.value = Math.tan(print.value);
  } else if (x === 'log'){
  		print.value = Math.log(print.value);
  } else {
    print.value += x;
  }
};

function evil(fn) {
  return new Function('return ' + fn)();
}

$(document).ready(function() {
	if (Storage) {

		//simpan semua tema
		localStorage.themes = `[
			{"id":0,"text":"Red","bcgColor":"#F44336","fontColor":"#0036f6"},
			{"id":1,"text":"Pink","bcgColor":"#E91E63","fontColor":"#0036f6"},
			{"id":2,"text":"Purple","bcgColor":"#9C27B0","fontColor":"#0036f6"},
			{"id":3,"text":"Indigo","bcgColor":"#3F51B5","fontColor":"#0036f6"},
			{"id":4,"text":"Blue","bcgColor":"#2196F3","fontColor":"#212121"},
			{"id":5,"text":"Teal","bcgColor":"#009688","fontColor":"#212121"},
			{"id":6,"text":"Lime","bcgColor":"#CDDC39","fontColor":"#212121"},
			{"id":7,"text":"Yellow","bcgColor":"#FFEB3B","fontColor":"#212121"},
			{"id":8,"text":"Amber","bcgColor":"#FFC107","fontColor":"#212121"},
			{"id":9,"text":"Orange","bcgColor":"#FF5722","fontColor":"#212121"},
			{"id":10,"text":"Brown","bcgColor":"#795548","fontColor":"#0036f6"}
		]`;

		//simpan tema default sekarang
		localStorage.selectedTheme = JSON.stringify({"id":3,"text":"Indigo","bcgColor":"#3F51B5","fontColor":"#0036f6"}); 
		
		// ubah tema yang dipilih sekarang
		changeBodyTheme(JSON.parse(localStorage.selectedTheme));

		$('.my-select').select2({
			'data' : JSON.parse(localStorage.themes)
		});
	}
	else {
		console.log("Local storage tidak tersedia")
	}
});

$('.apply-button').on('click', function(){ 
    var themeId = $(".my-select").val();
    var selectedTheme = JSON.parse(localStorage.themes)[themeId];
    changeBodyTheme(selectedTheme);
    
    localStorage.selectedTheme = JSON.stringify(selectedTheme);
});

function changeBodyTheme(theme) {
	$('body').css({
    	"background" : theme.bcgColor,
    	"color" : theme.fontColor
    });
}
